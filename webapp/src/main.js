// Import Vue app and components
import Vue from 'vue'
import App from './App.vue'
import Login from './components/Login.vue'
import MainPage from './components/MainPage.vue'
import Home from './components/Home.vue'

// Use timeago for filter
import timeago from 'timeago.js'

// Navigation item components
import NavItem from './components/navigation/NavItem.vue'
import SideReport from './components/navigation/reports/SideReport.vue'
import ReportList from './components/navigation/reports/ReportList.vue'
import ReportDetail from './components/navigation/reportdetail/ReportDetail.vue'

// Register components
Vue.component('login', Login);
Vue.component('mainpage', MainPage);
Vue.component('nav-item', NavItem);
Vue.component('home', Home);
Vue.component('sidereport', SideReport);
Vue.component('reportlist', ReportList);
Vue.component('reportdetail', ReportDetail);

// Register report status filter to map enum values and their named values.
Vue.filter('rstatus', function (statusID) {
  switch (statusID) {
    case -1: return 'Test Report';
    case 0: return 'Submitted';
    case 1: return 'In Progress';
    case 2: return 'Closed';
  }
});

// Register timeago filter.
Vue.filter('dateago', function (value) {
  return new timeago().format(value);
});

// Helper functions for storing a JSON object in localStorage.
Storage.prototype.setObject = function(key, value) {
  this.setItem(key, JSON.stringify(value));
}

Storage.prototype.getObject = function(key) {
  var value = this.getItem(key);
  if (!value) return;
  return JSON.parse(value);
}

new Vue({
  el: '#app',
  render: h => h(App)
})
