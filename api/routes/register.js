/* -----------------
File: routes/register.js
Description: The API endpoint for registering a new user.
----------------- */

var express = require('express');
var database = require('../common/database');
var bcrypt = require('bcrypt-nodejs');
var auth = require('../common/auth.js');
var router = express.Router();

var emailRegex = /^\S*@\S*\.\S*$/g;

router.get('/getParks', (req, res, next) => {
  database.getParks(function(err, parks) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
      next();
    }
    dictionary = {};
    for (i = 0; i < parks.length; ++i) {
      dictionary[parks[i].Managed_ID] = parks[i].Department;
    }
    res.json(dictionary);
  });
});

router.post('/', (req, res, next) => {
  var creds = req.body;

  if (!('username' in creds) && !('password' in creds) && !('email' in creds)) {
    res.json({
      success: false,
      message: 'Invalid credentials received.'
    });
    next();
  }

  else if (('managementId' in creds) && creds.managementId != 0 && !('passkey' in creds))
  {
    res.json({
      success: false,
      message: 'Invalid credentials received.'
    });
    next();
  }

  if (creds.username.length < 4 || creds.password.length < 8 || creds.email.length < 4) {
    res.json({
      success: false,
      message: 'The provided username, password and email do not meet the requirements.'
    });
    next();
  }

  // TODO: Fix the unreliable email regex
  // // Check the email is valid
  // if (!emailRegex.test(creds.email)) {
  //   res.json({ success: false, message: 'The provided email address is invalid.' });
  //   next();
  // }

  database.checkPassKey(creds.managementId, creds.passkey, (err, matches) => {
    if (err) {
      res.sendStatus(500);
      next();
    } else {
      if (!matches) {
         res.json({
            success: false,
            message: 'Invalid registration passkey.'
          });
          next();
      } else {
        database.checkUserExists(creds, (err, exists) => {
          if (err) {
            console.log(err);
            res.sendStatus(500);
            next();
          } else {
            if (!exists) {
              creds.password = bcrypt.hashSync(creds.password);
              database.registerNewUser(creds, (err, user) => {
                if (err) {
                  console.log(err);
                  res.sendStatus(500);
                  next();
                } else
                  res.json({
                    success: true,
                    token: auth.generateToken(user),
                    username: user.Username
                  });
              });
            } else {
              res.json({
                success: false,
                message: 'A user with that username or email address already exists.'
              });
              next();
            }
          }
        });
      }
    }
  });

});

module.exports = router;