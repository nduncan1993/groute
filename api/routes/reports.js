/* -----------------
File: routes/report.js
Description: The API endpoints for viewing and managing reports.
----------------- */

var express = require('express');
var database = require('../common/database');
var constants = require('../common/constants');
var auth = require('../common/auth.js');
var jwt = require('express-jwt');

var router = express.Router();

//returns spherical distance in miles
function latLonDistance(lat1, lon1, lat2, lon2) {
  var R = 6371000; // radius of earth in meters
  //convert to Radians
  var NSangle1 = lat1 * Math.PI / 180;
  var NSangle2 = lat2 * Math.PI / 180;
  var DNSangle = (lat2 - lat1) * Math.PI / 180;
  var DEWangle = (lon2 - lon1) * Math.PI / 180;
  //haversine formula
  var a = Math.sin(DNSangle / 2) * Math.sin(DNSangle / 2) + Math.cos(NSangle1) * Math.cos(NSangle2) * Math.sin(DEWangle / 2) * Math.sin(DEWangle / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = (R * c) / 1609.344; //convert to miles
  return d;
}

function reportStatusOpen (report) {
  return report.Status != 2;
}

router.use(jwt({ secret: constants.secret }));

router.get('/', (req, res, next) => {
  database.getAllReports(function (err, reports) {
    if (err) { console.log(err); res.sendStatus(500); return; }

    console.log(req.query.filter + ' ' + req.query.filter == 'true');

    if (req.query.filter == 'true' || req.query.filter == 'yes') {
      console.log('filtering');
      reports = reports.filter(reportStatusOpen);
    }

    res.json(reports);
  });
});

router.get('/ranged', (req, res, next) => {
  database.getAllReports(function (err, reports) {
    if (err) { console.log(err); res.sendStatus(500); return; }
        var latitude = req.query.lat;
        var longitude = req.query.lon;
        var radius = req.query.r;  
        filteredReports = [];
        for(i = 0;i < reports.length; ++i){
            if(latLonDistance(reports[i].Latitude,reports[i].Longitude,latitude,longitude) < radius){
                filteredReports.push(reports[i]);
            }
        }

        if (req.query.filter == 'true' || req.query.filter == 'yes') {
          console.log('filtering');
          filteredReports = filteredReports.filter(reportStatusOpen);
        }

        res.json(filteredReports);
  });
});


router.get('/:report_id', (req, res, next) => {
  database.getReport(req.params.report_id, function (err, detailedReport) {
    if (err) { console.log(err); res.sendStatus(500); return; }
        res.json(detailedReport);
  });
});

// returns the comments of a report.
router.get('/:report_id/comments', (req, res, next) => {
  database.getComments(req.params.report_id, function (err, comments) {
    if (err) { console.log(err); res.sendStatus(500); return; }
        res.json(comments);
  });
});

// endpoint /reports/comments creates a new comment related to a report
router.post('/:report_id/comments', (req, res, next) => {
  var commentData = req.body;

  if (!('comment' in commentData)) {
    res.json({ success: false, message: 'Invalid comment submitted.' });
    return;
  }

  database.insertComment(commentData, req.params.report_id, req.user, function (err) {
    if (err) { console.log(err); res.sendStatus(500); return; }
    else 
      res.json({ success: true, message: 'Added comment successfully.' });
  });
});

// endpoint /reports creates a new report
router.post('/', (req, res, next) => {
  var report = req.body;

  if (!('latitude' in report) && !('longitude' in report) && !('description' in report)) {
    res.json({ success: false, message: 'Invalid report submitted.' });
    return;
  }

  // Colorado is bounded by latitude and longitude... thankfully
  if (report.latitude > 41.0 || report.latitude < 37.0 || report.longitude < -109.03 || report.longitude > -102.03) {
    res.json({ success: false, message: 'Reports must be submitted within the boundaries of Colorado.' });
    return;
  }

  database.insertReport(report, req.user, function (err) {
    if (err) { console.log(err); res.sendStatus(500); return; }
    else 
      res.json({ success: true, message: 'Added report successfully.' });
  });
});

// Updates the status of a report.
router.put('/:report_id', (req, res, next) => {

try{

  var report = req.body;

  if (!('status' in report)) {
      res.json({ success: false, message: 'Invalid status submitted.' });
      return;
  }

  report.reportId = req.params.report_id;

  database.updateStatus(report, function (err, exists) {
    if (err) { console.log(err); res.sendStatus(500); return; }
    else 
      res.json({ success: exists, message: exists ? 'Updated Status successfully.' : 'Invalid report ID.' });
  });
}catch(err){console.log(err)};
});

module.exports = router;