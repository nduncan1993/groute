/* -----------------
File: routes/login.js
Description: The API endpoints for logging in as an existing user.
----------------- */

var express = require('express');
var bcrypt = require('bcrypt-nodejs');
var database = require('../common/database');
var auth = require('../common/auth.js');

var router = express.Router();

router.post('/', (req, res, next) => {
  var creds = req.body;
  if (!('username' in creds) && !('password' in creds)) {
      res.json({ success: false, message: 'Invalid credentials received.' });
      return;
  }

  database.getUser(creds.username, (err, user) => {
    if (err) { console.log(err); res.sendStatus(500); return; }

    if (user != null && bcrypt.compareSync(creds.password, user.Password))
      res.json({ success: true, token: auth.generateToken(user), username: user.Username });
    else
      res.json({ success: false, message: 'Incorrect username or password.' })
  }); 
});

module.exports = router;