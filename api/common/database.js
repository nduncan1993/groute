/* -----------------
File: database.js
Description: A location of all reusable database code components.
----------------- */

var mysql = require('mysql');

// Hack to allow easy switching between dev and prod without modifying our connection string
var debugEnabled = require('debug')('api:server').enabled;

var pool = mysql.createPool({
    connectionLimit : 20,
    host     : debugEnabled ? 'pwsvr1.pathwatchapp.com' : 'localhost',
    user     : 'Admin',
    password : 'B!Cycl3$afety',
    database : 'Groute'
});

module.exports = {
    queries: {
        // Login and registration
        userLogin: 'SELECT User_ID, Username, Password, Type FROM USER WHERE LOWER(Username) = ?',
        userRegister: 'INSERT INTO USER (Username, Password, Type, Email, Management_ID) VALUES (?, ?, ?, ?, ?)',
        checkUserExists: 'SELECT 1 as `EXISTS` FROM USER WHERE LOWER(Username) = ? OR LOWER(Email) = ?',
        checkPasskey: 'SELECT 1 as `MATCHES` FROM GOVERNMENT_AGENCY WHERE Managed_ID = ? AND Passkey = ?',

        // Retrieving reports
        getParkPairs: 'SELECT Managed_ID, Department FROM GOVERNMENT_AGENCY',
        getAllReports: 'SELECT Report_ID, Latitude, Longitude, RDate, Problem_description, Status FROM REPORT',
        getReport: 'SELECT r.Report_ID, r.Problem_description, r.RDate, u.Username, r.Status, r.Latitude, r.Longitude FROM REPORT r INNER JOIN USER u ON r.RUser_ID = u.User_ID WHERE Report_ID = ?',
        getComments: 'SELECT c.Comment, c.CDate, u.Username FROM INTERNAL_COMMENT c INNER JOIN USER u ON c.CUser_ID = u.User_ID WHERE CReport_ID = ?',

        // Modifying reports
        insertReport: 'INSERT INTO REPORT (Latitude, Longitude, Problem_description, RUser_ID) VALUES (?, ?, ?, ?)',
        insertComment: 'INSERT INTO INTERNAL_COMMENT (Comment, CUser_ID, CReport_ID) VALUES (?, ?, ?)',
        checkReportExists: 'SELECT 1 as `EXISTS` FROM REPORT WHERE Report_ID = ?',
        updateStatus: 'UPDATE REPORT SET Status = ? WHERE Report_ID = ?'
    },

    getUser: function (username, callback) {
        try {
            pool.query(this.queries.userLogin, [username.toLowerCase()], (err, rows, fields) => {
                if (!err) {
                    if (rows.length > 0)
                        callback(null, rows[0]);
                    else
                        callback(null, null);
                } else {
                    callback(err, null);
                }
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    checkUserExists: function (userObject, callback) {
        try {
            pool.query(this.queries.checkUserExists, [userObject.username, userObject.email], (err, rows, fields) => {
                if (!err) {
                        if (rows.length > 0) {
                            callback(null, true);
                        } else {
                            callback(null, false);
                        }
                    } else {
                        callback(err, null);
                    }
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    registerNewUser: function (userObject, callback) {
        try {
            userObject.type = 0;
            if (userObject.managementId != 0) userObject.type = 1;

            pool.query(this.queries.userRegister, [userObject.username, userObject.password, userObject.type, userObject.email, userObject.managementId], (err, rows, fields) => {
                // Return the same info as a login call so the user gets logged in immediately.
                pool.query(this.queries.userLogin, [userObject.username], (err, rows, fields) => {
                    if (!err) {
                        callback(null, rows[0]);
                    } else {
                        callback(err, null);
                    }
                });
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    getAllReports: function (callback) {
        try {
            pool.query(this.queries.getAllReports, (err, rows, fields) => {
                callback(err, rows);
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    getReport: function (reportId, callback) {
        try {
            pool.query(this.queries.getReport, [reportId], (err, rows, fields) => {
                callback(err, rows[0]);
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    getComments: function (reportId, callback) {
        try {
            pool.query(this.queries.getComments, [reportId], (err, rows, fields) => {
                callback(err, rows);
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
    },


    insertReport: function (report, user, callback) {
        try {
            pool.query(this.queries.insertReport, [report.latitude, report.longitude, report.description, user.userID], (err, rows, fields) => {
                callback(err);
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    insertComment: function (commentData, reportID, user, callback) {
        try {
            pool.query(this.queries.insertComment, [commentData.comment, user.userID, reportID], (err, rows, fields) => {
                callback(err);
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    updateStatus: function (report, callback) {
        try {
            pool.query(this.queries.checkReportExists, [report.reportId], (err, rows, fields) => {
                if (!err) {
                    if (rows.length > 0) {
                         var query = pool.query(this.queries.updateStatus, [report.status, report.reportId], (err, rows, fields) => {
                             console.log(err);
                            callback(null, true);
                        });

                        console.log(query);
                    } else {
                        callback(null, false);
                    }
                } else {
                    callback(err, null);
                }
            });      
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    getParks: function (callback) {
        try {
            pool.query(this.queries.getParkPairs, (err, rows, fields) => {
                callback(err, rows);
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    checkPassKey: function (managementId, passkey, callback) {
        if (!managementId || managementId === 0) {
            callback(null, true);
            return;
        }

        try {
            pool.query(this.queries.checkPasskey, [managementId, passkey], (err, rows, fields) => {
                if (!err) {
                        if (rows.length > 0) {
                            callback(null, true);
                        } else {
                            callback(null, false);
                        }
                    } else {
                        callback(err, null);
                    }
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
    }
}
