/* -----------------
File: constants.js
Description: A common location of all application constants.
----------------- */

module.exports = {
    appPort: 3124,
    appSocket: '/var/run/nodejs/pwapi.sock',
    secret: 'CD53A190FC0A359CEA66214EDB31958A43F27A2A635619D760A074C4F5C7AAF2'
}