/* -----------------
File: common/auth.js
Description: Common functionality for authentication
----------------- */

var jwt = require('jsonwebtoken');
var constants = require('./constants');

var exports = module.exports = {};

exports.generateToken = function (user) {
    return jwt.sign({ admin: user.Type === 1,
     				  userID: user.User_ID,
     				  managementId: user.managementId
                    },
                     constants.secret);
};

exports.verifyToken = function (token, callback) {
    jwt.verify(token, constants.secret, function(err, decoded) {
        if (err) { console.log(err); callback(err, false); }
        else {
            callback(null, true);
        }
    });
};